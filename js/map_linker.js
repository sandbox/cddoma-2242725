(function ($) { // IIAF
  Drupal.behaviors.map_linker_mapit = {
    attach: function (context, settings) {
      context = $(context);
      // find the set of mapit links and attach 'click' handler to them
      var mapit = context.find('.field-widget-link-field a.map-it');
      mapit.bind('click', function(event) {
        var element = $(this);
        var parent = element.closest('.field-widget-link-field');
        var title =  parent.find('.link-field-title .form-text');
        var url   =  parent.find('.link-field-url   .form-text');
        // prepend to the value of "title" and assign to "url"
        url.val("https://maps.google.com/?q=" + title.val());
        // prevent the default processing of the event
        return false;
      });
    }
  }
}(jQuery));
